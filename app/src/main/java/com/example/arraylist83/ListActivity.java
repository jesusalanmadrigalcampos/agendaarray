package com.example.arraylist83;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto> filter;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout)findViewById(R.id.tbtLista);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>)bundleObject.getSerializable("contactos");
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    public void buscar(String s)
    {
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x = 0; x < filter.size(); x++)
        {
            if (filter.get(x).getNombre().contains(s))
            {
                list.add(filter.get(x));
            }
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void cargarContactos()
    {
        for(int x=0; x < contactos.size(); x++)
        {
            final Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);
            Button btnBorrar = new Button(ListActivity.this);
            btnBorrar.setText(R.string.accborrar);
            btnBorrar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnBorrar.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c = (Contacto)v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", (Contacto) v.getTag(R.string.contacto_g));
                    oBundle.putSerializable("listaContactos", filter);
                    oBundle.putInt("index", (int) c.getiD());
                    oBundle.putBoolean("nuevo", false);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();

                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int y = 0; y < filter.size(); y++)
                    {
                        if(filter.get(y).getiD() == (int) c.getiD())
                        {
                            filter.remove(filter.get(y));
                            contactos = filter;
                            break;
                        }
                    }
                    tblLista.removeAllViews();
                    cargarContactos();
                }
            });


            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g, c.getiD());
            btnBorrar.setTag(R.string.contacto_g, c);
            btnBorrar.setTag(R.string.contacto_g, c.getiD());
            nRow.addView(nButton);
            nRow.addView(btnBorrar);
            tblLista.addView(nRow);
        }
    }
}
